import std.stdio;

void main()
{
    windowTest();

    writeln("done");
}

void windowTest()
{
	import std.typecons : Yes, No;

	import viewport.layer1.window;
	import viewport.layer1.eventloop;

	auto window = new Window("Vroom!", 720, 480, No.createHidden);

	while (!window.isCloseRequested)
	{
		EventLoop.processEvents(Yes.waitForEvents);
	}

	window.destroy();
}
