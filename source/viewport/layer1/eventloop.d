module viewport.layer1.eventloop;

struct EventLoop
{
    import std.typecons : Flag;
    import viewport.layer2.eventloop;

    static void processEvents(Flag!"waitForEvents" waitForEvents)
    {
        platformProcessEvents(waitForEvents);
    }
}
