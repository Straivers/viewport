module viewport.layer1.window;

struct WindowCreateParams
{
    import std.typecons : Flag;

    string title;
    ushort width, height;
    Flag!"createHidden" hidden;
}

class Window
{
    import viewport.layer2.window;
    import std.typecons : Flag;

    this()(auto ref in WindowCreateParams params)
    {
        auto rCreate = platformCreateWindow(params.title, params.width, params.height, params.hidden);
        _context = rCreate.unwrap();
        platformSetUserPointer(_context.handle, &_context);
    }

    this(in string title, ushort width, ushort height, Flag!"createHidden" createHidden)
    {
        this(WindowCreateParams(title, width, height, createHidden));
    }

    @property bool isCloseRequested()
    {
        return _context.isCloseRequested;
    }

private:

    WindowContext _context;
}
