module viewport.layer3.win32window;

version (Windows)  : import core.sys.windows.windows;
import viewport.layer0.result : Result;
import viewport.layer2.window : maxTitleWindowLength, WindowErrors;
import viewport.layer2.window : WindowPtr, WindowContext;

static immutable WNDCLASSEXW wndclass;
static immutable wndclassName = "viewport_win32_wndclass_name"w;

static this()
{
    WNDCLASSEXW wc;
    wc.cbSize = WNDCLASSEXW.sizeof;
    wc.lpfnWndProc = &windowProc;
    wc.hInstance = GetModuleHandle(null);
    wc.lpszClassName = &wndclassName[0];

    wndclass = cast(immutable) wc;

    RegisterClassExW(&wndclass);
}

Result!(WindowContext, WindowErrors) win32CreateWindow(in string title,
        ushort width, ushort height, bool hidden)
{
    import viewport.layer3.cinterop : tempCString;
    alias Result = typeof(return);

    auto tmpTitle = tempCString!(maxTitleWindowLength, wchar)(title);

    if (tmpTitle == null)
        return Result.err(WindowErrors.TitleTooLong);

    WindowContext context;

    //dfmt off
    HWND hwnd = CreateWindowExW(
        0,                              //Optional window styles
        &wndclassName[0],               //The name of the window class
        tmpTitle,                       //The name of the window
        WS_OVERLAPPEDWINDOW,            //Window Style
        CW_USEDEFAULT, CW_USEDEFAULT,   //(x, y) positions of the window
        width, height,   //The width and height of the window
        null,                           //Parent window
        null,                           //Menu
        GetModuleHandle(null),          //hInstance handle
        cast(void*) &context             //Additional application data
    );
    //dfmt on

    context.handle = hwnd;

    if (!hidden)
        win32ShowWindow(context.handle);

    if (hwnd == null)
        return Result.err(WindowErrors.WindowConstructionFailed);

    return Result.ok(context);
}

void win32DestroyWindow(WindowPtr handle)
{
    DestroyWindow(handle.ptr);
}

alias win32ShowWindow = win32SetWindowMode!SW_SHOW;
alias win32HideWindow = win32SetWindowMode!SW_HIDE;

void win32SetWindowMode(uint mode)(WindowPtr handle)
{
    ShowWindow(cast(void*) handle.ptr, mode);
}

void win32SetUserPointer(WindowPtr handle, WindowContext* context)
{
    SetWindowLongPtr(handle.ptr, GWLP_USERDATA, cast(LONG_PTR) context);
}

pragma(lib, "gdi32");
pragma(lib, "user32");

private extern (Windows) LRESULT windowProc(HWND hwnd, uint msg, WPARAM wp, LPARAM lp) nothrow
{
    import core.sys.windows.wingdi : GetStockObject;

    WindowContext* state;

    if (msg == WM_CREATE)
    {
        auto createStruct = cast(CREATESTRUCT*) lp;
        state = cast(WindowContext*) createStruct.lpCreateParams;
        SetWindowLongPtr(hwnd, GWLP_USERDATA, cast(LONG_PTR) state);
        state.handle = hwnd;
    }
    else
    {
        state = cast(WindowContext*) GetWindowLongPtr(hwnd, GWLP_USERDATA);
    }

    switch (msg)
    {
    case WM_CLOSE:
        state.isCloseRequested = true;
        return 0;

    case WM_DESTROY:
        assert(hwnd !is null);
        assert(state.handle.ptr == hwnd);

        PostQuitMessage(0);
        return 0;

    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hwnd, &ps);

            FillRect(hdc, &ps.rcPaint, GetStockObject(BLACK_BRUSH));

            EndPaint(hwnd, &ps);
        }
        return 0;

    default:
        return DefWindowProc(hwnd, msg, wp, lp);
    }
}
