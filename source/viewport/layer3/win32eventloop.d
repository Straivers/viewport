module viewport.layer3.win32eventloop;

import std.typecons: Flag;
import core.sys.windows.windows;

void win32ProcessEvents(Flag!"waitForEvents" shouldWait)
{
    MSG msg;

    if (shouldWait)
    {
        GetMessage(&msg, null, 0, 0);
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    while (PeekMessage(&msg, null, 0, 0, PM_REMOVE))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
}
