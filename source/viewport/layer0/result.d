module viewport.layer0.result;

struct Result(OkType, ErrType)
{

    static Result ok()(auto ref OkType okPayload)
    {
        Result result;
        result._okPayload = okPayload;
        result._isOk = true;
        return result;
    }

    static Result err()(auto ref ErrType errorPayload)
    {
        Result result;
        result._errorPayload = errorPayload;
        result._isOk = false;
        return result;
    }

    @property bool isOk()
    {
        return _isOk;
    }

    @property ErrType error()
    {
        return _errorPayload;
    }

    OkType unwrap()
    {
        if (_isOk)
        {
            return _okPayload;
        }

        assert(false, genUnwrapErrorMsg(this));
    }

private:
    bool _isOk;

    union
    {
        OkType _okPayload;
        ErrType _errorPayload;
    }

    string genUnwrapErrorMsg(Result result)
    {
        import std.conv : text, to;

        enum prologue = text("Result!(", OkType.stringof, ", ", ErrType.stringof, ") Error: ");
        //TODO: pregenerate messages if ErrType is an enum
        return text(prologue, result._errorPayload.to!string);
    }
}

@safe unittest
{
    auto result = Result!(uint, uint).err(32);

    assert(result.genUnwrapErrorMsg(result) == "Result!(uint, uint) Error: 32");
    assert(!result.isOk);
}
