module viewport.layer0.size;

struct Size
{
    uint x;
    uint y;

    alias width = x;
    alias height = y;
}
