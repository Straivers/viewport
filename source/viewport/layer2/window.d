module viewport.layer2.window;

enum maxTitleWindowLength = 2048;

enum WindowErrors
{
    NoError,
    ParameterMismatch,
    WindowConstructionFailed,
    TitleTooLong,
}

enum WindowMode
{
    Destroyed,
    Hidden,
    Minimized,
}

struct WindowContext
{
    WindowPtr handle;
    WindowMode mode;

    bool isCloseRequested;

    this(void* ptr)
    {
        handle = WindowPtr(ptr);
    }
}

struct WindowPtr
{
    this(void* ptr)
    {
        _ptr = ptr;
    }

    @property void* ptr() const pure nothrow
    {
        return cast(void*) _ptr;
    }

    void opAssign(void* ptr) pure nothrow
    {
        assert(ptr !is null);
        _ptr = ptr;
    }

    private void* _ptr;
}

//dfmt off
static immutable platformFunctions = [
    "CreateWindow",
    "DestroyWindow",

    "ShowWindow",
    "HideWindow",

    "SetUserPointer"
];
//dfmt on

version (Windows)
{
    import viewport.layer3.win32window;

    mixin(genPlatformAliases(platformFunctions, "win32"));
}

private string genPlatformAliases(in string[] functionNames, string platformPrefix)
{
    string result;

    foreach (name; functionNames)
    {
        result ~= "alias platform" ~ name ~ " = " ~ platformPrefix ~ name ~ ";\n";
    }

    return result;
}
