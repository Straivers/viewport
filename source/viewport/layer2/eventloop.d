module viewport.layer2.eventloop;

version (Windows)
{
    import viewport.layer3.win32eventloop;

    alias platformProcessEvents = win32ProcessEvents;
}
