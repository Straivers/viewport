# Design

Viewport was designed to be as simple to use and learn as possible, whilst allowing a developer to descend through the layers of abstraction provided seamlessly. The file structure of the project source reflects this, with the global `import viewport` exposing only the highest-level interfaces of the library.

Viewport is meant to be used as an assistance library. That is, it does not require that a program using Viewport follow certain conventions, like a custom entry point (`main` method).

The Viewport source is organized into layers, with the first layer (layer0) being the highest level, and the last layer (layer3) being the lowest.

### Principles of Design

- Reduce, reduce, reduce
    - If a function's processes does more than can be described in a single noun-verb-subject triplet, split it up into smaller functions. They do not necessarily have to be in a different function. Nested functions are ok too.
    - A function is a single unit of computation, and should behave atomically. A failure mid-function must not leave the program in undefined state.
- Delay decision making as much as possible
    - If a parameter or type can be decided at a later time, it probably should be.
    - Put new files in the project source directory (`viewport`). Move it at the end of its first iteration.
- Strictly enforce module boundaries. If a module requires initialization, create a struct of the same name to act as its context.
- Require as little as possible from the library's user
    - Specifying a memory allocator is optional.
    - Reasonable defaults are provided for most user-end code.

## Using Viewport as an Object-Oriented Library

A viewport GUI is structured with the `Viewport` at the top. It controls the renderer, and acts as an intermediate layer between user code and the Operating System. Within the `Viewport` are a list of views. Each `View` (not be confused with the model-view-controller pattern) is a drawing context, that holds information such as resizeability, color palette, event hooks, and a scenegraph of `UIComponent`s. `UIComponent`s are thinks like buttons, text, and scroll bars.

When constructing a GUI, you should typically only need to interact with `View`s and `UIComponent`s.

## Drawing with Viewport

Viewport natively supports four primitives:

- arcs,
- ellipses (like circles),
- points,
- and triangles.

Other, more complex shapes can be made by combining these primitives. For example, a rectangle may be created out of arcs, lines, or triangles.

Additionally, five attributes may be applied to each primitive:

- color,
- thickness (or size)
- transparency
- alpha-pattern (stripes, dotted lines, etc)
- anti-aliasing
- is mask/stencil

Shapes may also be used as stencils and masks, by pushing them onto a the draw stack. The draw stack is a list of commands, which are processed one at a time come render time.
