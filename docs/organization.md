# Code Organization

When in doubt, sort declarations in alphabetical order.

## Modules:

Declarations should be sorted in the following order:

1. Constants
2. Enums
3. Templates
5. Types
4. Functions

Functions should be sorted in order of expected use, with function pairs in the position of the first-used function.

    eg: 
        `loadVulkan` and `unloadVulkan` are at the top of the "functions" section of vulkan.d
