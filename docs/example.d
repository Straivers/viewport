module example;

import viewport;

void main()
{
    auto view = new View("Example", Size(1280, 720));
    //auto view = Viewport.loadComponent("viewport.dll")
    //                      .create("Example", Size(1280, 720));

    auto builder = SceneBuilder(view, Palette.defaultPalette);

    auto button = builder(view).buildButton()
                            .text("Button")
                            .onClick(e => e.drop())
                            .onHoover((e, self) => self.color = palette.alert)
                            .color(palette.foreground)
                            .create();

    builder.buildText("This is some text");

    view.enter();
}
