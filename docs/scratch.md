# Scratch
### This document is only for laying out thoughts and hammering out concepts before they are added to other doc pages. Anything and everything in this document is subject to change and may not be representative of the state of the project.


# Entrancy and the Event Loop

- The Viewport contains a window. If there is only a signle window, the event loop can belong to the window.
- This does not apply if more than one window in needed. A single program-global event loop is required.
    - Problem: If the viewport belongs to a different application (as a component) or if it is in a dll, a single event loop is a must. Not sure how this should be supported.

# Handling animations:

- Animations have a required tick rate
- Variable refresh-rate timer

# Allocating memory

- Delay decisions about where to allocate as much as possible.
- If nececssary, break apart a function to give the user as much control as possible.
    - Typically, prefer creating easily inline-able functions

# Rendering

- Starts with Viewport object
    - Viewport constructor has a UseHardwareRenderer flag
    - 